# Description

![Triffids Logo](logo-readme.png 'Triffids Logo')

**Triffids** is an application that celebrates the parks, green spaces and nature of Bristol by surfacing data from Bristol Open Data and other sources in a fun and engaging way.

Play with the app here -- [Triffids.app](https://triffids.app/)

## Development Information

To develop locally:

* Download the zip file from [GitLab](https://gitlab.com/bristol.codes/triffids)
* Unpack the archive
* `cd triffids`
* Follow the instructions in the next sections


## API
- [API Endpoints](https://gitlab.com/bristol.codes/triffids/wikis/API-Endpoints)

- [Data Spec](https://gitlab.com/bristol.codes/triffids/wikis/Data-Specification)

- Relies heavily on the [Open Data Bristol](https://opendata.bristol.gov.uk/pages/homepage/) API
  - [Parks and Green Spaces](https://opendata.bristol.gov.uk/explore/dataset/parks-and-greens-spaces/information/)
  - [Trees](https://opendata.bristol.gov.uk/explore/dataset/trees/information/)


## Server setup
Virtualenv recommended - more information [here](https://virtualenvwrapper.readthedocs.io/en/latest/install.html#basic-installation)

```
mkvirtualenv triffids
workon triffids
pip3 install -r env/requirements.txt
```


```
cd server
python3 server.py
```

## Front end setup
> Ensure you are using Node version >=8
```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

