import Vue from 'vue'
import Vuex from 'vuex'

import trees from './trees'
import parks from './parks'

Vue.use(Vuex)

const state = {
    hasGeoLocation: false,
    locationAllowed: false,
}

const getters = {
    hasGeoLocation: (st) => st.hasGeoLocation,
    locationAllowed: (st) => st.locationAllowed,
}

const mutations = {
    setHasGeoLocation(st, val) {
        st.hasGeoLocation = val
    },
    setLocationAllowed(st, val) {
        st.locationAllowed = val
    }
}

const actions = {}

export default new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    state,
    mutations,
    actions,
    getters,
    modules: {
        trees,
        parks,
    },
})