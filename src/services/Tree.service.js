import http from '../utils/http'
import Vue from 'vue';

export const treeService = {
    trees,
    tree,
}

/**
 * return an array of records for the trees found.
 *
 * empty if nothing found
 *
 * @param {string} site_code the code for the site
 */
async function trees(site_code) {
    const url = `${Vue.config.API_URL}/trees/${site_code}`;

    if (site_code === 'UNKNOWN') {
        return []
    }

    Vue.$log.info('Tree.service: loading trees: ', url)
    return http.get(url)
        .then((resp) => {
            return resp.data
        }, (err) => {
            throw err;
        });
}

async function tree(treeId) {
    const url = `${Vue.config.API_URL}/trees?id=${treeId}`;

    Vue.$log.info('Tree.service: loading tree: ', url)
    return http.get(url)
        .then((resp) => {
            return resp.data
        }, (err) => {
            throw err;
        });
}